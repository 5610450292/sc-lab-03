import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;


public class TestCase extends JFrame {
	//Char character ;
	private JLabel showProjectName;
	private JTextArea showResults;
	private String str;

	public static void main (String[] args) {
		TestCase test = new TestCase() ;
		test.createFrame();
		}
	public void setTestcase(){
		Char character = new Char () ;
		character.CharToAscii("https://www.facebook.com/") ;
		character.modAscii();
		setResult(character.toString()) ;
		character.CharToAscii("https://www.google.co.th/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("https://www.youtube.com/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("https://www.wikipedia.org/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("http://store.steampowered.com/?l=thai") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("http://www.pantip.com/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("http://www.seriesubthai.com/category/usa-series/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("https://bitbucket.org/account/signin/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("https://www.edmodo.com/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("https://www.apple.com/th/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("hhttp://www.garena.in.th/") ;
		character.modAscii();
		extendResult(character.toString()) ;
		character.CharToAscii("http://www.doojung.net/CategoriesMovie.php?menuId=2&&catId=1&&page=1") ;
		character.modAscii();
		extendResult(character.toString()) ;
	
	}

	public void createFrame() {
		this.showProjectName = new JLabel("URL THE SERIES");
		this.showResults = new JTextArea("your resutls will be showed here");
		setLayout(new BorderLayout()) ;
		setBounds(300,150,800,600) ;
		add(this.showProjectName , BorderLayout.NORTH);
		add(this.showResults , BorderLayout.CENTER);
		setVisible(true) ;
		setTestcase() ;
		
	}
	
	public void setProjectName(String s) {
		showProjectName.setText(s) ;
		
	}
	
	public void setResult(String str) {
		this.str = str;
		showResults.setText(this.str) ;
		
	}

	public void extendResult(String str) {
		this.str = this.str + "\n" + str ;
		showResults.setText(this.str) ;
		
	}

}
